import CourseCard from '../components/CourseCard';
// import courseData from '../data/courses.js';
import {Fragment, useEffect, useState} from 'react';
//we use Fragment kasi more that 1 yung ibabato nya na data
export default function Courses(){


const [courses, setCourses] = useState([]);
	// console.log(courseData);
	//Syntax:
		//localStorage.getItem("propertyName");

	// const local = localStorage.getItem("email");
	// console.log(local);

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_URL}/courses/allActiveCourses`)
		.then(response => response.json())
		.then(data =>{
			console.log(data);

			//sets the courses state to mapthe data retrieved from the fetch request into several "CourseCard" component
			setCourses(data.map(course =>{
		return(
			<CourseCard key = {course._id} prop = {course}/>
			)
	}))
		})
	},[])

	return(
		<Fragment>
		{courses}
		</Fragment>
		)
}