import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import {Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';

import UserContext from '../UserContext';
import Swal from 'sweetalert2';



export default function Login(){

const [email, setEmail] = useState ('');
const [password, setPassword] = useState ('');
const [isActive, setIsActive] = useState(false);

// const [user, setUser] = useState(null);

//Allows us o consume the User context object and its properties to use for user validation
const {user, setUser} = useContext(UserContext);

useEffect(() => {
	if(email !== "" && password !== ""){	
		setIsActive(true);
	}else{
		setIsActive(false);
	}
}, [email, password]);



function loginUser(event){
	event.preventDefault()

	// alert(`You are now Logged in!`)

	//Set the email of the autheticated user in the local Storage
		//Syntax
			//localStorage.setItem('propertyName', value)


	// set the global user state to have properties obtained from the local storag

	//through access to the user info can be done via the localstorage this is necessary to update the user state which will helpupdate the app component and rerend it ti avoid refershing the page upon user login and log out.

	
	// localStorage.setItem('email', email);
	// setUser(localStorage.getItem("email"));
	// setEmail('');
	// setPassword('');

	//Process wherein it will fetch a request to the corresponding API
	//"Content-Type" The header information, it is used to specify that the information being sent to the backend will be sent in the form of json
	//The fetch request will communicate with our backend application providing it with a stringified JSON

	//Syntax:
		//fetch('url', {options: method, header, body})
	//.then(replace => res.json())
	//.the(data =>{data process})

	fetch(`${process.env.REACT_APP_URL}/users/login`,{
		method: "POST",
		headers:{
			'Content-Type' : 'application/json',
		},
		body : JSON.stringify({
			email: email,
			password: password
		})
		
	}).then(response => response.json())
	.then(data => {
		//it is good to practice to always use or print out the result of our fetch request to ensure the correct information is recieved in our frontend application
		console.log(data);

		if (data.accessToken !== "empty") {
			localStorage.setItem('token', data.accessToken);
			retrieveUserDetails(data.accessToken);

		Swal.fire({
			title: "Login Successful",
			icon: "success",
			text: "Welcome to our website!"
		})
		}else{

			Swal.fire({
				title: "Aunthentication failed!",
				icon: "error",
				text: "Check you Login details and Try Again."
			})
			setPassword('');
		}
	})

	const retrieveUserDetails = (token) =>{
		fetch(`${process.env.REACT_APP_URL}/users/profile`,
			{
				headers: {
				Authorization: `Bearer ${token}`
			}})
			.then(response => response.json())
			.then(data => {
				console.log(data);

				setUser({id: data._id, isAdmin: data.isAdmin})
			})
	}

}



	return (
		(user.id !== null) ?
			<Navigate to = "/" />:

		<Container>
		<Row>
			<Col className = "col-md-4 col-8 offset-md-4 offset-2">
				<Form onSubmit = {loginUser} className = 'p-3'>
					<h1> Login </h1>
				  	<Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control 
				    	type="email" 
				    	placeholder="Enter email" 
				    	value = {email}
				    	onChange = {event => setEmail(event.target.value)}
				    	required/>
				  	</Form.Group>

					<Form.Group className="mb-3" controlId="password" >
					    <Form.Label>Password</Form.Label>
					    <Form.Control 
					    	type="password" 
					    	placeholder="Password" 
					    	value = {password}
					    	onChange = {event => setPassword(event.target.value)}
					    	required/>
					</Form.Group>
				  
				  	<Button variant="success" type="submit" disabled= {!isActive}>
				    Login
				  	</Button>

				</Form>
			</Col>

		</Row>
	    </Container>
	)
}