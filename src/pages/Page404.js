
import { Link } from 'react-router-dom';
import {Container, Row} from 'react-bootstrap';


export default function Logout(){

	localStorage.clear()
	return(
		<Container>
			<Row className = "text-center mt-5">
				<h1>ERROR 404</h1>
				<h1>Page Not Found!</h1>
				<Link to="/">Go back to Homepage!</Link>
			</Row>
		</Container>

	)

}